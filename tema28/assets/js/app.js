/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require("jquery");
require("bootstrap");
import Vue from 'vue';

var colors = new Vue({
    el:"#colors",
    delimiters:['{*','*}'],
    data:{
        background: '',
        // buttonStyle:{
        //     background: ''
        // }
        
   
    },
    methods:{
        
        randomColor :function(){ 
            this.background= '#'+ Math.floor(Math.random()*16777215).toString(16);
            console.log(this.background);
            return this.background;
        }
    }
});
