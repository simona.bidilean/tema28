<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ColorsController extends AbstractController
{
    /**
     * @Route("/colors", name="colors")
     */
    public function index()
    {
        return $this->render('colors/index.html.twig', [
            'controller_name' => 'ColorsController',
        ]);
    }
}
